# see also:  https://gist.github.com/TechplexEngineer/af6115adaa44567a1723

TARGET = TEMPLATE # change to desired name  # e.g. myboard for myboard.pcb
SCH_FILES = file1.sch file2.sch # change to a list of sch files

clean:
	/bin/rm -rf gerber/
	/bin/rm -f $(TARGET)_gerber.zip

gerber: 
	mkdir -p gerber
	/bin/rm -f $(TARGET)_gerber.zip
	pcb-rnd -x gerber --gerberfile gerber/$(TARGET) --all-layers --verbose $(TARGET).pcb
	./scripts/renamepcb_custom gerber
	zip $(TARGET)_gerber.zip gerber/$(TARGET).bottom.gbl gerber/$(TARGET).bottommask.gbs gerber/$(TARGET).bottomsilk.gbo gerber/$(TARGET).fab.gbr gerber/$(TARGET).outline.gko gerber/$(TARGET).plated-drill.xln gerber/$(TARGET).top.gtl gerber/$(TARGET).topmask.gts gerber/$(TARGET).topsilk.gto

pcbphoto:
	pcb-rnd -x png --dpi 1000 --photo-mode --photo-mask-colour green --photo-plating tinned --photo-silk-colour white --outfile $(TARGET)_render.png $(TARGET).pcb

pcbpng:
	pcb-rnd -x png --dpi 1000 --outfile $(TARGET)_board.png $(TARGET).pcb
pcbps:
	pcb-rnd -x ps --ps-color --psfile $(TARGET)_board.ps $(TARGET).pcb

pcbpdf: pcbps
	ps2pdf $(TARGET)_board.ps $(TARGET)_board.pdf

schpdf:
	gaf export -o $(TARGET)_sch.pdf $(SCH_FILES)

doc:  schpdf pcbpng pcbpdf pcbphoto 

docsch:  schpdf 

bom:
	ln -sfn attribs.all attribs
	gnetlist -g bom -o $(TARGET).bom $(SCH_FILES)
#	gnetlist -g partslist3 -o $(TARGET).bom $(SCH_FILES)
#	gnetlist -g partslist3 -o $(TARGET).bom *.sch

bomdigi:
	ln -sfn attribs.digi attribs
	gnetlist -g bom2 -o $(TARGET).bom $(SCH_FILES)


#tidy:
#	rm -f *~ *-
#	rm -f symbols/*~
#	rm -f footprints/*~

