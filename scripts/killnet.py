# reads in a netlist file and kills specific nets in that file


netsToKill = ['AGND','DGND','Vcc_reg','Vcc_cmp_reg']
filename = 'CLKGEN_TEST.net'

fh = open(filename,'r')
lines = fh.readlines()
fh.close()


fout = open(filename, 'w')
for line in lines:
    toks = line.split()
    if len(toks) == 0:
        continue
    killMe = False
    for net in netsToKill:
        if toks[0] == net:
            killMe = True

    if not killMe:
        fout.write(line)
fout.close()
