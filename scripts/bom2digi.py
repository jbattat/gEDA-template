# takes output of a bom2 bom and reformats for uploading to digikey

bomname = "CLKGEN_TEST.bom"
with open(bomname) as fh:
    # skip header
    line = fh.readline()
    for line in fh:
        # skip resistors, capacitors, inductors
        if line.startswith("R"):
            continue
        if line.startswith("C"):
            continue
        if line.startswith("L"):
            continue
        #print line,
        line = line.strip()
        toks = line.split(":")
        print "%5s, %40s" % (toks[2], toks[1])
        #print toks[1] + ", " + toks[2]
