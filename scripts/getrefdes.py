#filename = "CLKGEN_TEST.pcb"
#
#with open(filename, 'r') as f:
#  for line in f:
#      line = line.strip()
#      if not line.startswith("Element"):
#          continue
#      if line.startswith("ElementLine"):
#          continue
#      if line.startswith("ElementArc"):
#          continue
#      #print line
#      toks1 = line.split("[")
#      toks2 = toks1[1].split()
#      print "%15s"*2 % (toks2[1], toks2[2])

#filename = "APOLLO_ACS_LaserSlicer_ClockGen.sch"
filename = "APOLLO_ACS_LaserSlicer_CPLD.sch"
MISSING = "=============MISSING=============="

incomponent = False
foundRefdes = False

components = []
refdess    = []

with open(filename, 'r') as f:
  for line in f:
    line = line.strip()
    #print line
    if incomponent:
      while True:
        if line.find('refdes') > -1:
          print "here"  
          refdess.append(line)
          print "found refdes"
          foundRefdes = True
        elif line == "}":
          print "here2"  
          if not foundRefdes:
            refdess.append(MISSING)
          foundRefdes = False
        break
      
    if line.startswith("C "):
      if line.find("DGND.sym") > -1:
        continue
      if line.find("vcc-1.sym") > -1:
        continue
      incomponent = True
      components.append(line)
      continue

for c in components:
    print c
for r in refdess:
    print r

for ii in range(len(components)):
    if refdess[ii] == MISSING:
        print "%50s"*2 % (components[ii], refdess[ii])

print len(components)
print len(refdess)
